package test_01_hanoi;

public class MainTest {

	public static void main(String[] args) {

		hanoi("A", "B", "C", 4);

	}

	public static void hanoi(String from, String by, String to, int level) {
		if (level == 1)
			System.out.println("from " + from + " to " + to);
		else {
			hanoi(from, to, by, level - 1);
			System.out.println("from " + from + " to " + to);
			hanoi(by, from, to, level - 1);
		}
	}
}
