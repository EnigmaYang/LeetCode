package __letter_combinations_of_a_phone_number;

import java.util.List;

public class MainTest {

	public static void main(String[] args) {

		List<String> result = new Solution().letterCombinations("23");
		for (String str : result)
			System.out.println(str);

	}

}
