package __h_index;

import java.util.TreeMap;

public class Solution {
	public int hIndex(int[] citations) {
		TreeMap<Integer, Integer> tm = new TreeMap<Integer, Integer>();
		for (int i = 0; i < citations.length; i++)
			if (tm.containsKey(citations[i]))
				tm.put(citations[i], tm.get(citations[i]) + 1);
			else
				tm.put(citations[i], 1);

		int tmVal = 0;
		int maxResult = 0;
		for (int tmKey : tm.descendingKeySet()) {
			tmVal += tm.get(tmKey);
			maxResult = tmKey > tmVal ? (tmVal > maxResult ? tmVal : maxResult)
					: (tmKey > maxResult ? tmKey : maxResult);
		}

		return maxResult;
	}
}
