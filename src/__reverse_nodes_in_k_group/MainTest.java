package __reverse_nodes_in_k_group;

import __reverse_nodes_in_k_group.Solution;

public class MainTest {

	public static void main(String[] args) {
		ListNode head = init(987654321);
		printListNode(head);

		ListNode result = new Solution().reverseKGroup1(head, 4);
		printListNode(result);
	}

	private static void printListNode(ListNode l) {
		if (l == null)
			return;

		while (l.next != null) {
			System.out.print(l.val + " -> ");
			l = l.next;
		}
		System.out.println(l.val);

	}

	private static ListNode init(int i) {
		ListNode result = new ListNode(i % 10);
		ListNode pointer = result;
		i = i / 10;
		while (i > 0) {
			ListNode temp = new ListNode(i % 10);
			i = i / 10;
			pointer.next = temp;
			pointer = temp;
		}
		return result;
	}

	static class ListNode {

		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}

	}

}
