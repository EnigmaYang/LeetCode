package __reverse_nodes_in_k_group;

import __reverse_nodes_in_k_group.MainTest.ListNode;

public class Solution {

	public ListNode reverseKGroup1(ListNode head, int k){
		if(k == 0 || k == 1) return head;
        ListNode cur = head;
        int length = 0;
        while (cur != null){
            cur = cur.next;
            length++;
        }
        int multi = length / k;
        if(multi == 0) return head;
        ListNode preTail = null, curHead = null, curTail = null;
        ListNode preNode = null, nextNode = null;
        cur = head;
        for(int j = 0; j < multi; j++) {
            preNode = null;
            for(int i = 0; i < k; i++) {
                if(cur != null) {
                    nextNode = cur.next;
                    cur.next = preNode;
                    preNode = cur;
                }
                if(i == 0) curTail = cur;
                if(i == (k - 1)) curHead = cur;
                cur = nextNode;
            }
            if(preTail == null) head = curHead;
            else preTail.next = curHead;
            preTail = curTail;
        }
        return head;
	}
	
	
	public ListNode reverseKGroup(ListNode head, int k) {
		ListNode counterPointer = head;
		int counter = 0;

		while (counterPointer != null) {
			counterPointer = counterPointer.next;
			counter++;
		}

		if (counter == 0 || counter < k) {
			return head;
		}

		ListNode originalStart = head;
		ListNode moveFrom = head;
		ListNode moveTo = head;
		ListNode remindPointer = head.next;

		for (int i = 1; i < k; i++) {

			moveTo = remindPointer;
			remindPointer = remindPointer.next;
			moveTo.next = moveFrom;
			moveFrom = moveTo;

		}

		head = moveTo;
		originalStart.next = remindPointer;

		return head;
	}

}
