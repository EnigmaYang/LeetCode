package _200_299._278_first_bad_version;

public class Solution extends VersionControl {
	public int firstBadVersion(int n) {
		int start = 1;
		int end = n;

		int current = 1 + (n - 1) / 2;

		while (start < end) {

			if (isBadVersion(current))
				end = current - 1;
			else
				start = current + 1;
			current = start + (end - start) / 2;
		}

		if (isBadVersion(current))
			return current;
		else
			return current + 1;
	}

}

class VersionControl {
	int version = 1702766719;

	boolean isBadVersion(int version) {
		return version - this.version > 0 ? true : false;
	}
}
