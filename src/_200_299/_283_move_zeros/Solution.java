package _200_299._283_move_zeros;

public class Solution {

	public void moveZeroes(int[] nums) {

		if (nums.length < 2)
			return;

		int zeroPointer = nums.length - 1;

		while (nums[zeroPointer] == 0) {
			zeroPointer--;
			if (zeroPointer == 0)
				return;
		}

		for (int endPointer = zeroPointer - 1; endPointer >= 0; endPointer--)
			if (nums[endPointer] == 0) {
				for (int pointer = endPointer; pointer < zeroPointer; pointer++) {
					nums[pointer] = nums[pointer + 1];
				}
				nums[zeroPointer] = 0;
			}
	}

	public void moveZeroes2(int[] nums) {
		int fromPointer = 0;
		int toPointer = 0;
		while (fromPointer < nums.length) {
			if (nums[fromPointer] == 0)
				fromPointer++;
			else
				nums[toPointer++] = nums[fromPointer++];
		}

		for (int i = toPointer; i < nums.length; i++)
			nums[i] = 0;
	}

}
