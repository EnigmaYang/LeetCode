package _200_299._283_move_zeros;

import java.util.Arrays;

public class MainTest {

	public static void main(String[] args) {

		int[] nums = { 0, 1, 0, 3, 12, 0, 0, 1, 0 };

		new Solution().moveZeroes2(nums);

		System.out.println(Arrays.toString(nums));

	}

}
