package _200_299._242_validanagram;

public class MainTest {

	public static void main(String[] args) {

		String s = "assdfffggs";
		String t = "ssdaffggfe";

		System.out.println(new Solution().isAnagram(s, t));

	}

}
