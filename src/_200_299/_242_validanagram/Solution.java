package _200_299._242_validanagram;

class Solution {
	public boolean isAnagram(String s, String t) {
		if (s.length() != t.length())
			return false;

		int[] s_counter = new int[26];
		int[] t_counter = new int[26];

		char[] s_c = s.toCharArray();
		char[] t_c = t.toCharArray();

		for (int i = 0; i < s_c.length; i++) {
			s_counter[s_c[i] - 97]++;
			t_counter[t_c[i] - 97]++;
		}

		for (int i = 0; i < 26; i++) {
			if (s_counter[i] != t_counter[i])
				return false;
		}

		return true;

	}
}