/**
 * 
 */
package _200_299._268_missing_number;

/**
 * @author Yawei Yang
 * 
 * 
 */
public class Solution {
    public int missingNumber(int[] nums) {
        int sum=nums.length;
        for(int i=0;i<nums.length;i++){
            sum=sum-nums[i]+i;
        }
        return sum;
    }
}
