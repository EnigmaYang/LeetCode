package __search_a_2d_matrix_ii;

public class MainTest {

	public static void main(String[] args) {

		int[][] matrix = new int[][] { { 1, 4, 7, 11, 15 }, { 2, 5, 8, 12, 19 }, { 3, 6, 9, 16, 22 },
				{ 10, 13, 14, 17, 24 }, { 18, 21, 23, 26, 30 } };
		int target = 20;

		System.out.println(new Solution().searchMatrix(matrix, target));

	}

}
