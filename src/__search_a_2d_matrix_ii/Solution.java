package __search_a_2d_matrix_ii;

public class Solution {

	public boolean searchMatrix(int[][] matrix, int target) {

		int width = matrix[0].length;
		int height = matrix.length;

		int x = 0;
		int y = width - 1;

		while (x < height) {
			if (matrix[x][y] > target) {
				System.out.println("x: " + x + "  y: " + y + "  value: " + matrix[x][y]);
				while (y > 0 && y < width && matrix[x][y] > target)
					y--;
				if (y < 0)
					y++;
				if(matrix[x][y] < target)
					x++;
				else
					return true;
			} else if (matrix[x][y] < target) {
				System.out.println("x: " + x + "  y: " + y + "  value: " + matrix[x][y]);
				while (y > 0 && y < width && matrix[x][y] < target)
					y++;
				if ( y >= width)
					y--;
				if(matrix[x][y] > target)
					x++;
				else
					return true;
			} else
				return true;
		}
		return false;

	}
}
