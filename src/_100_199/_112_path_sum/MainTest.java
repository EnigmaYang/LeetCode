package _100_199._112_path_sum;

public class MainTest {

    public static void main(String[] args) {
        Solution solution = new Solution();

//        TreeNode node5 = new TreeNode(5);
//        TreeNode node4 = new TreeNode(4);
//        TreeNode node8 = new TreeNode(8);
//        TreeNode node11 = new TreeNode(11);
//        TreeNode node13 = new TreeNode(13);
//        TreeNode node4_2 = new TreeNode(4);
//        TreeNode node7 = new TreeNode(7);
//        TreeNode node2 = new TreeNode(2);
//        TreeNode node1 = new TreeNode(1);
//
//        node5.left = node4;
//        node5.right = node8;
//
//        node4.left = node11;
//
//        node11.left = node7;
//        node11.right = node2;
//
//        node8.left = node13;
//        node8.right = node4_2;
//
//        node4_2.right = node1;

        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(-2);
        TreeNode node3 = new TreeNode(-3);
        TreeNode node4 = new TreeNode(1);
        TreeNode node5 = new TreeNode(3);
        TreeNode node6 = new TreeNode(-2);
        TreeNode node7 = new TreeNode(-1);

        node1.left = node2;
        node1.right = node3;

        node2.left = node4;
        node2.right = node5;

        node3.left = node6;
        node4.left = node7;


        System.out.println(solution.hasPathSum(node1, 1));
    }

}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
