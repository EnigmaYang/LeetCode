package _100_199._112_path_sum;

import sun.reflect.generics.tree.Tree;

import java.util.ArrayList;
import java.util.Stack;

public class Solution {


    public boolean hasPathSum(TreeNode root, int sum) {

        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root;
        while (null != node || !stack.empty()) {
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
            if (!stack.empty()) {
                node = stack.peek();
                if (null != node.right) {
                    stack.push(node.right);
                    node = node.right;
                } else {
                    TreeNode pop = stack.pop();
                    System.out.println(pop.val);

                }
            }

        }


        return false;



        /*int currSum = 0;

        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur;
        TreeNode pre = null;
        stack.push(root);
        currSum += root.val;
        while (!stack.empty()) {
            cur = stack.peek();
            if ((cur.left == null && cur.right == null) ||
                    (pre != null && (pre == cur.left || pre == cur.right))) {
                System.out.println(cur.val + "");
                TreeNode pop = stack.pop();


                pre = cur;
            } else {
                if (cur.right != null) {
                    stack.push(cur.right);
                    currSum += cur.right.val;
                }
                if (cur.left != null) {
                    stack.push(cur.left);
                    currSum += cur.left.val;
                }
            }
        }
        return false;*/













        /*Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root;
        while (node != null || !stack.empty()) {
            while (node != null) {


//                if(stack.empty()){
//                    stack.push(new TreeNode(0));
//                }
//                node.val += stack.peek().val;
                System.out.println(node.val);
                stack.push(node);
                node = node.left;
            }
            if (!stack.empty()) {
                node = stack.pop();
//                System.out.println(node.val);

                node = node.right;
            }
        }
        return false;*/
    }


//    private boolean result = false;
//
//    public boolean hasPathSum(TreeNode root, int sum) {
//        return hasPathSum(root, sum, 0);
//    }
//
//    public boolean hasPathSum(TreeNode node, int target, int currentSum) {
//        if (null == node) {
//            return false;
//        }
//
//        if (null == node.left && null == node.right && node.val + currentSum == target) {
//            return true;
//        }
//
//        result = result || hasPathSum(node.left, target, currentSum + node.val);
//        if(result){
//            return true;
//        } else {
//            return hasPathSum(node.right, target, currentSum + node.val);
//        }
//    }
}