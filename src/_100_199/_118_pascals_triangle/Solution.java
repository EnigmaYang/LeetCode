package _100_199._118_pascals_triangle;

import java.util.ArrayList;
import java.util.List;

public class Solution {

	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		for (int i = 0; i < numRows; i++) {
			List<Integer> listInteger = new ArrayList<Integer>();
			for (int j = 0; j <= i; j++) {
				double resultVal = 1;
				for (int x = 1; x <= j; x++) {
					resultVal *= (i - x+1);
					resultVal /= x;
				}
				listInteger.add((int)Math.round(resultVal));
			}
			result.add(listInteger);
		}
		return result;
	}

}
