package _100_199._118_pascals_triangle;

import java.util.Iterator;
import java.util.List;

public class MainTest {

	public static void main(String[] args) {
		List<List<Integer>> result = new Solution().generate(25);
		printPascalTriangle(result);

	}

	public static void printPascalTriangle(List<List<Integer>> result) {
		Iterator<List<Integer>> iteratorList = result.iterator();
		while (iteratorList.hasNext()) {
			List<Integer> listInteger = iteratorList.next();
			Iterator<Integer> iteratorInteger = listInteger.iterator();
			while (iteratorInteger.hasNext()) {
				System.out.print(iteratorInteger.next());
				if (iteratorInteger.hasNext())
					System.out.print(", ");
			}
			System.out.println();
		}
	}

}
