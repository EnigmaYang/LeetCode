package _100_199._119_pascals_triangle_ii;

import java.util.ArrayList;
import java.util.List;

public class Solution {

	public List<Integer> generate(int rowIndex) {
		List<Integer> result = new ArrayList<Integer>();
		for (int j = 0; j <= rowIndex; j++) {
			double resultVal = 1;
			for (int x = 1; x <= j; x++) {
				resultVal *= (rowIndex - x + 1);
				resultVal /= x;
			}
			result.add((int) Math.round(resultVal));
		}
		return result;
	}

}
