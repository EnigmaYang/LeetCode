package _100_199._119_pascals_triangle_ii;

import java.util.Iterator;
import java.util.List;

public class MainTest {

	public static void main(String[] args) {
		List<Integer> result = new Solution().generate(25);
		printPascalTriangle(result);

	}

	public static void printPascalTriangle(List<Integer> result) {
		Iterator<Integer> iteratorInteger = result.iterator();
		while (iteratorInteger.hasNext()) {
			System.out.print(iteratorInteger.next());
			if (iteratorInteger.hasNext())
				System.out.print(", ");
		}
		System.out.println();
	}
}
