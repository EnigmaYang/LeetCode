package _100_199._142_linked_list_cycle_ii;

public class MainTest {

	public static void main(String[] args) {
		ListNode head0 = new ListNode(3);
		ListNode head1 = new ListNode(2);
		ListNode head2 = new ListNode(0);
		ListNode head3 = new ListNode(-4);
		head0.next = head1;
		head1.next = head2;
		head2.next = head3;
		head3.next = head1;

		System.out.println(new Solution().detectCycle(head0).val);
	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}
}