package _100_199._142_linked_list_cycle_ii;

public class Solution {
	public ListNode detectCycle(ListNode head) {
		ListNode p1 = head;
		ListNode p2 = head;
		do {
			if (p1 == null || p1.next == null)
				return null;
			p1 = p1.next.next;
			p2 = p2.next;
		} while (p1 != p2);
		p1 = head;
		while (p1 != p2) {
			p1 = p1.next;
			p2 = p2.next;
		}
		return p1;
	}
}
