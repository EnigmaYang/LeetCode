package _100_199._153_find_min_in_rotate;

public class Solution {

    public int findMin(int[] nums) {

        int min = 0;

        if (null == nums) {
            return min;
        }

        int leftIndex = 0;
        int midIndex = nums.length / 2;
        int rightIndex = nums.length - 1;

        while (leftIndex < rightIndex) {
            if (nums[midIndex] < nums[rightIndex]) {
                rightIndex = midIndex;
            } else if (nums[midIndex] > nums[rightIndex]) {
                leftIndex = midIndex + 1;
            }
            midIndex = (rightIndex + leftIndex) / 2;
        }
        return nums[rightIndex];
    }

//    public int findMin(int[] nums) {
//        int left = 0, right = nums.length - 1;
//        if (nums[left] < nums[right]) {
//            return nums[left];
//        }
//        while(right - left > 1) {
//            int middle = (left + right) / 2;
//            if (nums[middle] > nums[left]) {
//                left = middle;
//            } else {
//                right = middle;
//            }
//        }
//        return nums[right];
//    }

}
