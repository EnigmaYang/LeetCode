package _100_199._189_rotate_array;

import java.util.Arrays;

public class Solution {

	public static void rotate(int[] nums, int k) {
		
		k %= nums.length;

		int[] temp = Arrays.copyOfRange(nums, 0, nums.length - k);

		System.arraycopy(nums, nums.length - k, nums, 0, k);

		System.arraycopy(temp, 0, nums, k, nums.length - k);

	}

}
