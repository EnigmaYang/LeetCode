package ____200_number_of_islands;

public class Solution {
	static char[][] matrix = null;
	static int count = 0;
	static int cur_sum = 0;

	public int numIslands(char[][] grid) {
		matrix = new char[grid.length][grid[0].length];
		for (int i = 0; i < grid.length; i++)
			for (int j = 0; j < grid[0].length; j++)
				matrix = grid;
		// search for each element;
		for (int i = 0; i < grid.length; i++)
			for (int j = 0; j < grid[0].length; j++) {
				if (solution(i, j) > 0)
					count++;
			}
		System.out.println(count);
		return count;
	}

	private static int solution(int x, int y) {
		if (matrix[x][y] == 48)
			return 0;
		cur_sum = cur_sum + 1;
		matrix[x][y] = 0;
		if (x - 1 >= 0)
			solution(x - 1, y);
		if (y - 1 >= 0)
			solution(x, y - 1);
		if (x + 1 < matrix.length)
			solution(x + 1, y);
		if (y + 1 < matrix[0].length)
			solution(x, y + 1);
		return cur_sum;
	}
}
