package ____200_number_of_islands;

public class MainTest {

	public static void main(String[] args) {

		final int size = 5;
		char[][] grid = new char[size][size];

		// generate matrix
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				grid[i][j] = (char) ((int) Math.round(Math.random()) + 48);
				System.out.print(grid[i][j] + " ");
			}
			System.out.println();
		}

		System.out.println(new Solution().numIslands(grid));
	}

}
