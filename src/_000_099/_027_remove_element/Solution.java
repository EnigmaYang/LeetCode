package _000_099._027_remove_element;

class Solution {

	public int removeElement(int[] nums, int val) {
		int counter = nums.length;
		for (int i = 0; i < counter; i++)
			if (nums[i] == val)
				nums[i--] = nums[counter-- - 1];
		return counter;
	}

	public int removeElement2(int[] A, int elem) {
		int number = 0;
		for (int i = 0; i < A.length; i++)
			if (A[i] != elem)
				A[number++] = A[i];
		return number;
	}

}