package _000_099._027_remove_element;

public class MainTest {

	public static void main(String[] args) {

		int result = new Solution().removeElement(new int[] { 1, 2, 3, 2, 3, 1, 2, 3 }, 2);
		System.out.println(result);
		int result2 = new Solution().removeElement2(new int[] { 1, 2, 3, 2, 3, 1, 2, 3 }, 2);
		System.out.println(result2);

	}

}
