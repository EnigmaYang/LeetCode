package _000_099._003_longest_substring_without_repeating_characters;

public class Solution {

	public int lengthOfLongestSubstring(String s) {

		boolean[] set = new boolean[128];

		int position1 = 0;
		int position2 = 0;
		int maxLength = 0;

		while (position1 < s.length() && position2 < s.length()) {

			if (set[s.charAt(position1)] != true) {
				// if (set.add(s.charAt(position1))) {

				set[s.charAt(position1)] = true;
				position1++;
				maxLength = (position1 - position2) > maxLength ? (position1 - position2) : maxLength;

			} else {
				set[s.charAt(position2)] = false;
				// set.remove(s.charAt(position2));
				position2++;
			}

		}

		return maxLength;

	}

}
