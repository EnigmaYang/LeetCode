package _000_099._008_string_to_integer_atoi;

class Solution {
	public int myAtoi(String str) {
		str = str.trim();
		if (str == null || str.equals(""))
			return 0;
		boolean beginning = true;
		int sign = 1;
		double result = 0;
		int i = 0;
		if (str.charAt(0) == '+') {
			sign = 1;
			i = 1;
		} else if (str.charAt(0) == '-') {
			sign = -1;
			i = 1;
		}

		for (; i < str.length(); i++) {
			if (str.charAt(i) > '9' || str.charAt(i) < '0')
				if (result * sign < Integer.MIN_VALUE)
					return Integer.MIN_VALUE;
				else if (result * sign > Integer.MAX_VALUE)
					return Integer.MAX_VALUE;
				else
					return (int) (result * sign);
			if (beginning == true && str.charAt(i) == '0') {
				continue;
			} else {
				beginning = false;
				result *= 10;
				result += str.charAt(i) - '0';

			}
		}
		if (result * sign < Integer.MIN_VALUE)
			return Integer.MIN_VALUE;
		else if (result * sign > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;
		else
			return (int) (result * sign);
	}
}