package _000_099._036_valid_sudoku;

public class MainTest {

	public static void main(String[] args) {

		char[][] board = new char[9][9];

		String[] boardStr = new String[] { ".46...6..", "...6....4", ".....1...", ".....7...", "5.7...4..", "........3",
				"...7..1..", ".........", "..12....." };

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++)
				board[i][j] = boardStr[i].charAt(j);
		}

		System.out.println(new Solution().isValidSudoku(board));

	}

}
