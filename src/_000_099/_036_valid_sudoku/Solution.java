package _000_099._036_valid_sudoku;

public class Solution {

	public boolean isValidSudoku(char[][] board) {

		boolean[] set = new boolean[10];

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != '.')
					if (set[board[i][j] - 48] != true) {
						set[board[i][j] - 48] = true;
					} else {
						return false;
					}
			}
			for (int j = 1; j < 10; j++)
				set[j] = false;
		}

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[j][i] != '.')
					if (set[board[j][i] - 48] != true) {
						set[board[j][i] - 48] = true;
					} else {
						return false;
					}
			}
			for (int j = 1; j < 10; j++)
				set[j] = false;
		}

		for (int i = 0; i < 9; i = i + 3) {
			for (int j = 0; j < 9; j = j + 3) {
				for (int x = i; x < i + 3; x++)
					for (int y = j; y < j + 3; y++) {
						if (board[x][y] != '.')
							if (set[board[x][y] - 48] != true) {
								set[board[x][y] - 48] = true;
							} else {
								return false;
							}
					}
				for (int z = 1; z < 10; z++)
					set[z] = false;
			}

		}

		return true;

	}

}
