package _000_099._021_merge_tow_sorted_lists;

public class MainTest {

	public static void main(String[] args) {
		ListNode l1 = new ListNode(1);
		ListNode l2 = new ListNode(2);
		ListNode l3 = new ListNode(3);
		ListNode l4 = new ListNode(4);
		ListNode l5 = new ListNode(5);
		ListNode l6 = new ListNode(6);
		ListNode l7 = new ListNode(7);
		ListNode l8 = new ListNode(8);
		ListNode l9 = new ListNode(9);

		l1.next = l3;
		l2.next = l4;
		l3.next = l5;
		l4.next = l6;
		l5.next = l7;
		l6.next = l8;
		l7.next = l9;

		 System.out.println(new Solution().mergeTwoLists(l1, l2));

//		System.out.println(l1);

	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}

	@Override
	public String toString() {
		if (next == null)
			return val + " ";
		return val + " " + next.toString();

	}

}
