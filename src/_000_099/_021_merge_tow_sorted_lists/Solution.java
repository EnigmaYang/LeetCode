package _000_099._021_merge_tow_sorted_lists;

/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode
 * next; ListNode(int x) { val = x; } }
 */

public class Solution {

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {

		ListNode p1 = l1;
		ListNode p2 = l2;

		ListNode result = new ListNode(0);
		ListNode resPointer = result;

		if (p1 == null)
			return p2;
		if (p2 == null)
			return p1;

		while (p1 != null && p2 != null) {
			if (p1.val < p2.val) {
				resPointer.next = p1;
				p1 = p1.next;
			} else {
				resPointer.next = p2;
				p2 = p2.next;
			}
			resPointer = resPointer.next;
		}

		if (p1 == null)
			resPointer.next = p2;
		else
			resPointer.next = p1;
		return result.next;
	}

}
