package _000_099._061_rotate_list;

import _000_099._061_rotate_list.MainTest.ListNode;

public class Solution {

	public ListNode rotateRight(ListNode head, int k) {
		if (head == null || head.next == null)
			return head;

		ListNode counterPointer = head;
		int counter = 1;

		while (counterPointer.next != null) {
			counterPointer = counterPointer.next;
			counter++;
		}
		
		k %= counter;

		if (k == 0)
			return head;
		ListNode originalStart = head;
		ListNode moveTo = head;

		

		for (int i = 0; i < counter - k - 1; i++) {
			moveTo = moveTo.next;
		}

		ListNode moveFrom = moveTo.next;

		head = moveFrom;
		counterPointer.next = originalStart;
		moveTo.next = null;

		return head;
	}

}
