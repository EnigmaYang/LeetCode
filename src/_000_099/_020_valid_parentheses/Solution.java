package _000_099._020_valid_parentheses;

import java.util.Stack;

public class Solution {
	public boolean isValid(String s) {
		if (s == null)
			return true;
		char[] cs = s.toCharArray();
		if (cs.length % 2 != 0)
			return false;
		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < cs.length; i++) {

			switch (cs[i]) {
			case '{':
			case '[':
			case '(':
				stack.push(cs[i]);
				break;
			default:
				if (stack.isEmpty())
					return false;
				char temp = stack.pop();
				if (cs[i] != (temp + 2) && cs[i] != (temp + 1))
					return false;
			}
		}
		return stack.isEmpty();
	}
}
