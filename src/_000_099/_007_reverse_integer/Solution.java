package _000_099._007_reverse_integer;

class Solution {
	public int reverse(int x) {

		double result = 0;
		while (x % 10 != 0) {
			result = result * 10 + x % 10;
			x = x / 10;
		}
		if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE)
			return 0;
		else
			return (int) result;
	}

}