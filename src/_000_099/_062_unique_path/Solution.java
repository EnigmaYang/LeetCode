package _000_099._062_unique_path;

public class Solution {

	public int uniquePaths(int m, int n) {

		int max = m + n - 2;
		int min = (m < n ? m : n) - 1;
		int result = 1;

		
		for (int i = 0; i < min; i++) {
			result *= (max - i);
			System.out.println(result);
		}
		for (int i = 1; i <= min; i++) {
			result /= i;
		}
		return (int) result;
	}

}
