package _000_099._064_minimum_path_sum._061_rotate_list;

public class MainTest {

    public static void main(String[] args) {

        int[][] grid = {{1, 3, 1},
                {1, 5, 1},
                {4, 2, 1}};

        System.out.println(new Solution().minPathSum(grid));

    }

}
