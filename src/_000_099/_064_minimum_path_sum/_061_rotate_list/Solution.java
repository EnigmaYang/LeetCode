package _000_099._064_minimum_path_sum._061_rotate_list;

public class Solution {
    public int minPathSum(int[][] grid) {


        if (null == grid || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }

        for (int i = 1; i < grid[0].length; i++) {
            grid[0][i] = grid[0][i] + grid[0][i - 1];
        }

        for (int i = 1; i < grid.length; i++) {
            grid[i][0] = grid[i][0] + grid[i - 1][0];
        }

        for (int i = 1; i < grid.length; i++) {
            for (int j = 1; j < grid[0].length; j++) {
                grid[i][j] = grid[i][j] + min(grid[i - 1][j], grid[i][j - 1]);
            }
        }


        return grid[grid.length - 1][grid[0].length - 1];
    }

    private int min(int v1, int v2) {
        if (v1 < v2) {
            return v1;
        } else {
            return v2;
        }
    }
}