package _000_099._002_add_two_numbers;

import _000_099._002_add_two_numbers.MainTest.ListNode;

public class Solution {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int c = 0;
		ListNode result = new ListNode(0);
		ListNode pointer = result;

		while (l1 != null && l2 != null) {
			ListNode temp = new ListNode((l1.val + l2.val + c) % 10);
			c = (l1.val + l2.val + c) / 10;

			l1 = l1.next;
			l2 = l2.next;
			pointer.next = temp;
			pointer = temp;
		}

		if (l1 != null) {
			pointer.next = l1;
		} else if (l2 != null) {
			pointer.next = l2;
		}

		while (pointer.next != null) {
			pointer = pointer.next;
			int temp = (pointer.val + c) % 10;
			c = (pointer.val + c) / 10;
			pointer.val = temp;

		}

		if (c == 1) {
			pointer.next = new ListNode(1);
		}

		return result.next;
	}

}
