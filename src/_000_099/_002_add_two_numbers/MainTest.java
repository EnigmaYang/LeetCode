package _000_099._002_add_two_numbers;

public class MainTest {

	public static void main(String[] args) {
		ListNode l1 = init(12349);
		ListNode l2 = init(52345);
		printListNode(l1);
		printListNode(l2);

		ListNode result = new Solution().addTwoNumbers(l1, l2);
		printListNode(result);
	}

	private static void printListNode(ListNode l) {
		while (l.next != null) {
			System.out.print(l.val + " -> ");
			l = l.next;
		}
		System.out.println(l.val);

	}

	private static ListNode init(int i) {
		ListNode result = new ListNode(i % 10);
		ListNode pointer = result;
		i = i / 10;
		while (i > 0) {
			ListNode temp = new ListNode(i % 10);
			i = i / 10;
			pointer.next = temp;
			pointer = temp;
		}
		return result;
	}

	static class ListNode {

		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}

	}

}
