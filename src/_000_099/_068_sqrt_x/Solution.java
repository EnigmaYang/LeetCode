package _000_099._068_sqrt_x;

public class Solution {

	public int mySqrt(int x) {

		method1(x);

		method2(x);

		return method3(x);
	}

	private int method3(int x) {
		if (x == 0)
			return 0;

		int xCopy = x;
		int counter = 0;
		while (xCopy != 0) {
			counter++;
			xCopy = xCopy >> 1;
		}

		int[] result = new int[10];
		result[0] = x >> (counter / 2);
		for (int i = 1; i < 10; i++) {
			result[i] = (result[i - 1] + x / result[i - 1]) / 2;
			if (result[i] == result[i - 1])
				return result[i];
			else if (i > 2 && result[i] == result[i - 2])
				return result[i] < result[i - 1] ? result[i] : result[i - 1];
		}
		return 0;
	}

	private int method2(int x) {
		int res = 0;
		for (int mask = 1 << 15; mask != 0; mask >>>= 1) {
			int next = res | mask; // set bit
			if (next <= x / next)
				res = next;
		}
		return res;
	}

	private int method1(int x) {
		long r = x;
		while (r * r > x) {
			r = (r + x / r) / 2;
		}
		return (int) r;
	}

}
