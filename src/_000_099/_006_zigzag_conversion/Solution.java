package _000_099._006_zigzag_conversion;

class Solution {
	public String convert(String s, int numRows) {

		StringBuilder[] sNumRows = new StringBuilder[numRows];
		for (int i = 0; i < numRows; i++)
			sNumRows[i] = new StringBuilder();
		int counter1 = 0;
		while (counter1 < s.length() / numRows) {
			if (counter1 * 2 * (numRows - 1) + 0 < s.length())
				sNumRows[0].append(s.charAt(counter1 * 2 * (numRows - 1) + 0));
			if (counter1 * 2 * (numRows - 1) + numRows - 1 < s.length())
				sNumRows[numRows - 1].append(s.charAt(counter1 * 2 * (numRows - 1) + numRows - 1));
			counter1++;
		}

		for (int i = 1; i < numRows - 1; i++) {

		}

		return null;
	}

	public String convert2(String s, int numRows) {
		if (numRows == 1)
			return s;

		char[][] res = new char[numRows][s.length() / 2 + numRows];
		int x = 0;
		int y = 0;
		boolean flag = true;

		for (int i = 0; i < s.length(); i++) {
			if (x == numRows) {
				flag = false;
				x = x - 2;
				y++;
			} else if (x == -1) {
				flag = true;
				x = x + 2;
				y--;
			}
			res[x][y] = s.charAt(i);
			if (flag) {
				x++;
			} else {
				x--;
				y++;
			}
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < s.length() / 2 + numRows; j++)
				if (res[i][j] != '\0' && res[i][j] != ' ')
					sb.append(res[i][j]);
		}
		return sb.toString();
	}

}