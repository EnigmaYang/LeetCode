package _000_099._045_jump_game_ii;

class Solution {

	public int jump3(int[] nums) {
		int steps = 0;
		int current = 0;
		int next = 0;
		for (int i = 0; i < nums.length; i++) {
			if (i <= current) {
				next = Math.max(next, i + nums[i]);
			} else if (i <= next) {
				current = next;
				steps++;
				i--;
			} else {
				return Integer.MAX_VALUE;
			}
		}
		return steps;
	}

	public int jump2(int[] nums) {

		int[] result = new int[nums.length];
		for (int i = 0; i < nums.length; i++) {
			for (int j = 1; j <= nums[i]; j++) {
				if ((i + j) < nums.length && result[i + j] == 0) {
					result[i + j] = result[i] + 1;
				}
			}
			if (result[nums.length - 1] != 0)
				return result[nums.length - 1];
		}
		return Integer.MAX_VALUE;
	}

	public int jump(int[] nums) {
		int steps = 0;
		int current = 0, next = 0;
		for (int i = 0; i < nums.length; i++) {
			if (i <= current) {
				next = Math.max(next, i + nums[i]);
			} else if (i <= next) {
				current = next;
				steps++;
				i--;
			} else {
				return Integer.MAX_VALUE;
			}

		}

		return steps;
	}
}