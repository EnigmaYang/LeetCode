package _000_099._045_jump_game_ii;

import java.util.Date;

public class MainTest {

	public static void main(String[] args) {

		// int[] array = new int[25001];
		// for (int i = 0; i < 25001; i++)
		// array[i] = 25000 - i;

		int[] array = new int[] { 2, 2, 1, 0, 4 };

		long timer1 = new Date().getTime();
		int result = new Solution().jump(array);
		System.out.println("jump" + " " + String.valueOf(new Date().getTime() - timer1));
		System.out.println(result);

		long timer2 = new Date().getTime();
		int result2 = new Solution().jump2(array);
		System.out.println("jump2 " + String.valueOf(new Date().getTime() - timer2));
		System.out.println(result2);
		// int result = new Solution().jump(new int[] { 2, 3, 1, 1, 4 });

	}

}
