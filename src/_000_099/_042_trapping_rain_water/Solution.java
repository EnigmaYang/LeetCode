package _000_099._042_trapping_rain_water;

public class Solution {
	public int trap(int[] height) {

		int[] rainFilled = new int[height.length];

		if (height == null || height.length == 0)
			return 0;

		int max = height[0];
		int position = 0;

		// find the last height peak into position
		for (int i = 0; i < height.length; i++)
			if (height[i] >= max) {
				max = height[i];
				position = i;
			}
		// fill the rain

		rainFilled[0] = height[0];
		for (int i = 1; i <= position; i++) {
			if (height[i] < rainFilled[i - 1])
				rainFilled[i] = rainFilled[i - 1];
			else
				rainFilled[i] = height[i];
		}

		rainFilled[height.length - 1] = height[height.length - 1];
		for (int i = height.length - 2; i > position; i--) {
			if (height[i] > rainFilled[i + 1])
				rainFilled[i] = height[i];
			else
				rainFilled[i] = rainFilled[i + 1];
		}

		int result = 0;
		for (int i = 0; i < height.length; i++)
			result += rainFilled[i] - height[i];
		return result;
	}

}
