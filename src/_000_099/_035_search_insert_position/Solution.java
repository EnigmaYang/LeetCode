package _000_099._035_search_insert_position;

public class Solution {
    public int searchInsert(int[] nums, int target) {
        if (null == nums) {
            return 0;
        }

        int leftIndex = 0;
        int midIndex = nums.length / 2;
        int rightIndex = nums.length - 1;

        while (leftIndex < rightIndex) {

            if (nums[midIndex] < target) {
                leftIndex = midIndex + 1;
            } else if (nums[midIndex] > target) {
                rightIndex = midIndex - 1;
            } else {
                rightIndex = midIndex;
            }
            midIndex = (rightIndex + leftIndex) / 2;
        }


        if (nums[midIndex] < target) {
            midIndex += 1;
        }
        return midIndex;
    }
}
