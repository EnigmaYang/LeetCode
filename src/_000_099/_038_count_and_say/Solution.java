package _000_099._038_count_and_say;

public class Solution {

	public String countAndSay(int n) {
		String number = "1";
		StringBuilder stringBuilder = new StringBuilder();

		for (int i = 0; i < n - 1; i++) {
			char currentChar = number.charAt(0);
			int currentTimes = 0;
			for (int index = 0; index < number.length(); index++) {
				if (number.charAt(index) == currentChar) {
					currentTimes++;
				} else {
					stringBuilder.append(currentTimes);
					stringBuilder.append(currentChar);
					currentChar = number.charAt(index);
					currentTimes = 1;
				}
			}
			stringBuilder.append(currentTimes);
			stringBuilder.append(currentChar);
			number = stringBuilder.toString();
			stringBuilder.setLength(0);
		}
		return number;
	}

}
