package _000_099._019_remove_nth_node_from_end_of_list;

import _000_099._019_remove_nth_node_from_end_of_list.MainTest.ListNode;

public class Solution {

	public ListNode removeNthFromEnd(ListNode head, int n) {

		int length = 0;
		ListNode pointer = head;
		while (pointer != null) {
			pointer = pointer.next;
			length++;
		}

		int element = length - n;

		if (element == 0) {
			head = head.next;
		} else {
			pointer = head;
			for (int i = 0; i < element - 1; i++)
				pointer = pointer.next;
			pointer.next = pointer.next.next;
		}

		return head;
	}
}
