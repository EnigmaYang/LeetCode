package _000_099._041_first_missing_positive;

public class MainTest {

	public static void main(String[] args) {

		int result = new Solution().firstMissingPositive(new int[] { 1, 2, 3, 5, 6, 7, 8, 9 });
		System.out.println(result);

	}

}
