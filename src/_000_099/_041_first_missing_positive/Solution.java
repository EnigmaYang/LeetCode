package _000_099._041_first_missing_positive;

import java.util.ArrayList;

public class Solution {
	public int firstMissingPositive(int[] nums) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		for (int i = 0; i < nums.length; i++)
			a.add(nums[i]);

		for (int i = 1; i < Integer.MAX_VALUE; i++)
			if (a.contains(i))
				continue;
			else
				return i;
		return 0;

	}

}
