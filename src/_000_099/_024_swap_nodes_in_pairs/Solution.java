package _000_099._024_swap_nodes_in_pairs;

public class Solution {

	public ListNode swapPairs(ListNode head) {
		ListNode pointer;
		ListNode result = new ListNode(0);
		pointer = result;
		result.next = head;
		while (head != null && head.next != null) {
			head = head.next.next;
			pointer.next.next.next = pointer.next;
			pointer.next = pointer.next.next;
			pointer.next.next.next = head;
			pointer = pointer.next.next;
		}
		return result.next;
	}

}
