package _000_099._001_two_sum;

import java.util.Arrays;

class Solution {
	public int[] twoSum(int[] nums, int target) {
		int[] result = new int[2];
		for (int i = 0; i < nums.length; i++)
			for (int j = i + 1; j < nums.length; j++)
				if (nums[i] + nums[j] == target) {
					result[0] = i + 1;
					result[1] = j + 1;
					Arrays.sort(result);
					return result;
				}
		return null;
	}
}