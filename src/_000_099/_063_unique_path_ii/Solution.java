package _000_099._063_unique_path_ii;

public class Solution {

	public int uniquePathsWithObstacles(int[][] obstacleGrid) {

		int height = obstacleGrid.length;
		int width = obstacleGrid[0].length;

		if (obstacleGrid[0][0] == 1)
			return 0;
		obstacleGrid[0][0] = 1;

		int x = 0;
		int y = 0;
		int rowNumber = 0;
		for (int pointer = 1; pointer < width * height; pointer++) {

			x--;
			y++;
			if (x < 0 || y >= width) {
				rowNumber++;
				if (rowNumber >= height)
					x = height - 1;
				else
					x = rowNumber;
				y = rowNumber - x;
			}

			System.out.println(x + " " + y);

			if (obstacleGrid[x][y] == 1) {
				obstacleGrid[x][y] = 0;
			} else {
				if (y > 0)
					obstacleGrid[x][y] += obstacleGrid[x][y - 1];
				if (x > 0)
					obstacleGrid[x][y] += obstacleGrid[x - 1][y];
			}

		}

		return obstacleGrid[height - 1][width - 1];

	}

	public int uniquePathsWithObstacles2(int[][] obstacleGrid) {
		int height = obstacleGrid.length;
		int width = obstacleGrid[0].length;

		if (obstacleGrid[0][0] == 1)
			return 0;
		obstacleGrid[0][0] = 1;

		for (int x = 1; x < height; x++)
			obstacleGrid[x][0] = obstacleGrid[x - 1][0] & (~obstacleGrid[x][0]);

		for (int y = 1; y < width; y++)
			obstacleGrid[0][y] = obstacleGrid[0][y - 1] & (~obstacleGrid[0][y]);

		for (int x = 1; x < height; x++) {
			for (int y = 1; y < width; y++) {
				if (obstacleGrid[x][y] == 1)
					obstacleGrid[x][y] = 0;
				else {
					obstacleGrid[x][y] = obstacleGrid[x - 1][y] + obstacleGrid[x][y - 1];
				}
			}
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++)
				System.out.print(obstacleGrid[i][j]);
			System.out.println();
		}
		return obstacleGrid[height - 1][width - 1];

	}
}
