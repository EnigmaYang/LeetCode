package _000_099._063_unique_path_ii;

public class MainTest {

	public static void main(String[] args) {
		int[][] obstacleGrid = new int[][] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

		System.out.println(new Solution().uniquePathsWithObstacles2(obstacleGrid));
	}

}
