package _000_099._053_maximum_subarray;

public class Solution {
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxValue = nums[0];
        int preSumVal = nums[0];
        for (int i = 1; i < nums.length; i++) {

            if (preSumVal < 0) {
                preSumVal = nums[i];
            } else {
                preSumVal += nums[i];
            }

            if (preSumVal > maxValue) {
                maxValue = preSumVal;
            }
        }
        return maxValue;
    }
}
