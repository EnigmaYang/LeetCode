package __dungeon_game;

import __dungeon_game.Solution;

public class MainTest {

	public static void main(String[] args) {

		int[][] dungeon = new int[][] { { -2, -3, 3 }, { 10, -10, 1 }, { -990, 30, -5 } };
		

		System.out.println(new Solution().calculateMinimumHP2(dungeon));

		
	}

}
