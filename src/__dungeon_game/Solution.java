package __dungeon_game;

public class Solution {

	public int calculateMinimumHP(int[][] dungeon) {

		int[][] min = new int[dungeon.length][dungeon[0].length];
		min[0][0] = dungeon[0][0];

		for (int i = 1; i < dungeon[0].length; i++) {
			dungeon[0][i] += dungeon[0][i - 1];
			min[0][i] = dungeon[0][i] < dungeon[0][i - 1] ? dungeon[0][i] : dungeon[0][i - 1];
		}

		for (int i = 1; i < dungeon.length; i++) {
			dungeon[i][0] += dungeon[i - 1][0];
			min[i][0] = dungeon[i][0] < dungeon[i - 1][0] ? dungeon[i][0] : dungeon[i - 1][0];
		}

		for (int i = 1; i < dungeon[0].length; i++)
			for (int j = 1; j < dungeon.length; j++) {
				dungeon[i][j] += dungeon[i - 1][j] > dungeon[i][j - 1] ? dungeon[i - 1][j] : dungeon[i][j - 1];

				min[i][j] = dungeon[i][j] < dungeon[i - 1][0] ? dungeon[i][0] : dungeon[i - 1][0];

			}

		for (int[] a : dungeon) {
			for (int x : a)
				System.out.print(x + " ");
			System.out.println();
		}

		System.out.println("======================");

		for (int[] a : min) {
			for (int x : a)
				System.out.print(x + " ");
			System.out.println();
		}

		return dungeon[dungeon.length - 1][dungeon[0].length - 1];

	}

	public int calculateMinimumHP2(int[][] dungeon) {
		int m = dungeon.length;
		int n = dungeon[0].length;
		int[][] ans = new int[m][n];
		// 初始化最后一行和最后一列
		ans[m - 1][n - 1] = dungeon[m - 1][n - 1] > 0 ? 0 : -dungeon[m - 1][n - 1];
		for (int i = m - 2; i >= 0; i--) {
			ans[i][n - 1] = dungeon[i][n - 1] >= ans[i + 1][n - 1] ? 0 : ans[i + 1][n - 1] - dungeon[i][n - 1];
		}
		for (int j = n - 2; j >= 0; j--) {
			ans[m - 1][j] = dungeon[m - 1][j] >= ans[m - 1][j + 1] ? 0 : ans[m - 1][j + 1] - dungeon[m - 1][j];
		}
		// 从右下角往左上角遍历
		for (int i = m - 2; i >= 0; i--) {
			for (int j = n - 2; j >= 0; j--) {
				int down = dungeon[i][j] >= ans[i + 1][j] ? 0 : ans[i + 1][j] - dungeon[i][j];
				int right = dungeon[i][j] >= ans[i][j + 1] ? 0 : ans[i][j + 1] - dungeon[i][j];
				ans[i][j] = Math.min(down, right);
			}
		}
		
		
		
		for (int[] a : ans) {
			for (int x : a)
				System.out.print(x + " ");
			System.out.println();
		}

		
		
		// 要保证勇士活着，至少需要1魔力
		return ans[0][0] + 1;
	}

}
